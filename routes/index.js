var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {

  var GraphDataJavaRef = Java.type('in.silentsudo.GraphData');
  var graphData = new GraphDataJavaRef();
  var data = graphData.getPoints();
  res.render('index', { title: 'Express Tutorial', data: data});
});

module.exports = router;

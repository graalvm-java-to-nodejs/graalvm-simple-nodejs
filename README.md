## Run
Add java jar to node command line runner
```shell
/home/ashish/.sdkman/candidates/java/21.1.0.r11-grl/languages/nodejs/bin/node \
  --jvm --vm.cp=/home/ashish/IdeaProjects/graavlvm/java-lib-gvm/build/libs/java-lib-gvm-1.0-SNAPSHOT.jar \
  /home/ashish/IdeaProjects/graavlvm/express-sample/bin/www
```
